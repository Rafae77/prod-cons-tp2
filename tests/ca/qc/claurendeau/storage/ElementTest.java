package ca.qc.claurendeau.storage;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElementTest {
	Element element;
	
	@Before
	public void setUp() throws Exception {
		element = new Element();
	}

	@After
	public void tearDown() throws Exception {
		element = null;
	}

	@Test
	public void testElementDataEquals4() {
		element.setData(4);
		assertEquals(4, element.getData());
	}
	
	@Test
	public void testElementDataEquals5() {
		element.setData(5);
		assertEquals(5, element.getData());
	}

}
