package ca.qc.claurendeau.storage;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;

public class BufferTest {
	Buffer buffer;

	@Before
	public void setUp() throws Exception {
		buffer = new Buffer(3);
	}

	@After
	public void tearDown() throws Exception {
		buffer = null;
	}

	@Test
	public void testBufferCapacityEquals3() {
		assertEquals(3, buffer.capacity());
	}

	@Test
	public void testBufferCurrentLoad1() throws BufferFullException {
		Element element = new Element();
		buffer.addElement(element);
		assertEquals(1, buffer.getCurrentLoad());
	}

	@Test
	public void testBufferIsEmpty() {
		assert (buffer.isEmpty());
	}

	@Test
	public void testBufferIsEmptyAdding1Element() throws BufferFullException {
		Element element = new Element();
		buffer.addElement(element);
		assert (!buffer.isEmpty());
	}

	@Test
	public void testBufferIsFull() {
		assert (!buffer.isFull());
	}

	@Test
	public void testBufferIsFullAdding3Elements() throws BufferFullException {
		Element element1 = new Element();
		Element element2 = new Element();
		Element element3 = new Element();
		buffer.addElement(element1);
		buffer.addElement(element2);
		buffer.addElement(element3);
		assert (buffer.isFull());
	}

	@Test
	public void testBufferAddElement() throws Exception {
		Element element = new Element();
		buffer.addElement(element);
		assertEquals(1, buffer.getCurrentLoad());
	}

	@Test(expected = BufferFullException.class)
	public void testBufferFullExceptionMax3Elements() throws Exception {
		Element element1 = new Element();
		Element element2 = new Element();
		Element element3 = new Element();
		Element element4 = new Element();

		buffer.addElement(element1);
		buffer.addElement(element2);
		buffer.addElement(element3);
		buffer.addElement(element4);
	}
	
	@Test(expected = BufferEmptyException.class)
	public void testBufferEmptyExceptionDeleteElementEmptyBuffer() throws Exception {
		buffer.removeElement();
	}
	
	@Test
	public void testBufferRemoveElementFirstOne() throws Exception {
		Element element1 = new Element();
		Element element2 = new Element();

		buffer.addElement(element1);
		buffer.addElement(element2);
		
		assert(element1.equals(buffer.removeElement()));
	}
	
	@Test
	public void testBufferToStringEmptyBuffer() {
		assertEquals(" ",buffer.toString());
	}
	
	@Test
	public void testBufferToString1Element() throws BufferFullException {
		Element element1 = new Element();
		element1.setData(3);

		buffer.addElement(element1);
		assertEquals(" 3 ",buffer.toString());
	}
	
	@Test
	public void testBufferToStringVariousElement() throws BufferFullException {
		Element element1 = new Element();
		Element element2 = new Element();
		element1.setData(3);
		element2.setData(3);
		
		buffer.addElement(element1);
		buffer.addElement(element2);
		assertEquals(" 3 3 ",buffer.toString());
	}
}
