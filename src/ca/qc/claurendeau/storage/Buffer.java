package ca.qc.claurendeau.storage;

import java.util.ArrayList;
import java.util.List;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;

public class Buffer
{
    private int capacity;
    private final int firstElementPostion = 0;
    private List<Element> elements;

    public Buffer(int capacity)
    {
        this.capacity = capacity;
        this.elements = new ArrayList<Element>();
    }

    // returns the content of the buffer in form of a string
    public String toString()
    {
    	
        String string = " ";
        for (Element element : elements) {
        	string += element.getData() + " ";
		}
        return string;
    }

    // returns the capacity of the buffer
    public int capacity()
    {
        return capacity;
    }

    // returns the number of elements currently in the buffer
    public int getCurrentLoad()
    {
        // VOTRE CODE
        return elements.size();
    }

    // returns true if buffer is empty, false otherwise
    public boolean isEmpty()
    {
        // VOTRE CODE
        return getCurrentLoad() == 0 ? true : false;
    }

    // returns true if buffer is full, false otherwise
    public boolean isFull()
    {
        return getCurrentLoad() == this.capacity ? true : false;
    }

    // adds an element to the buffer
    // Throws an exception if the buffer is full
    public synchronized void addElement(Element element)  throws BufferFullException{
    	if(getCurrentLoad() >= this.capacity){
    		throw new BufferFullException();
    	}else{
    		this.elements.add(element);
    	}
    }
    
    // removes an element and returns it
    // Throws an exception if the buffer is empty
    public synchronized Element removeElement() throws BufferEmptyException {
    	Element elementEffacer;
    	if(this.elements.isEmpty()){
        	throw new BufferEmptyException();
        }else{
        	elementEffacer = elements.get(firstElementPostion);
        	elements.remove(firstElementPostion);
        }
        return elementEffacer;
    }
}